package com.crud.crud;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Utilisateurs implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id	@GeneratedValue(strategy=GenerationType.AUTO)
private int id;
@Column
private String nom;
@Column
private String prenom;
@Column
private int matricule;
@Column
private String adresse;
@Column
private String mail;
@Column
private String telephone;
@Column
private String service;
@Column
private String fonction;
@Column
private String mot_de_passe_identifiant;


@ManyToOne
@JoinColumn(name="Type_user")
private Type_user type_user;
public Utilisateurs() {
	super();
	// TODO Auto-generated constructor stub
}



public Utilisateurs( String nom, String prenom, int matricule, String adresse, String mail, String telephone,
		String service, String fonction, String mot_de_passe_identifiant, Type_user type_user) {
	super();
	
	this.nom = nom;
	this.prenom = prenom;
	this.matricule = matricule;
	this.adresse = adresse;
	this.mail = mail;
	this.telephone = telephone;
	this.service = service;
	this.fonction = fonction;
	this.mot_de_passe_identifiant = mot_de_passe_identifiant;
	this.type_user = type_user;
}



public Type_user getType_user() {
	return type_user;
}



public void setType_user(Type_user type_user) {
	this.type_user = type_user;
}



public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public String getNom() {
	return nom;
}

public void setNom(String nom) {
	this.nom = nom;
}

public String getPrenom() {
	return prenom;
}

public void setPrenom(String prenom) {
	this.prenom = prenom;
}

public int getMatricule() {
	return matricule;
}

public void setMatricule(int matricule) {
	this.matricule = matricule;
}

public String getAdresse() {
	return adresse;
}

public void setAdresse(String adresse) {
	this.adresse = adresse;
}

public String getMail() {
	return mail;
}

public void setMail(String mail) {
	this.mail = mail;
}

public String getTelephone() {
	return telephone;
}

public void setTelephone(String telephone) {
	this.telephone = telephone;
}

public String getService() {
	return service;
}

public void setService(String service) {
	this.service = service;
}

public String getFonction() {
	return fonction;
}

public void setFonction(String fonction) {
	this.fonction = fonction;
}

public String getMot_de_passe_identifiant() {
	return mot_de_passe_identifiant;
}

public void setMot_de_passe_identifiant(String mot_de_passe_identifiant) {
	this.mot_de_passe_identifiant = mot_de_passe_identifiant;
}



}


