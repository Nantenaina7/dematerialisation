package com.crud.crud.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import com.crud.crud.Fichier;
import com.crud.crud.dao.FichierRepository;



@Controller
@CrossOrigin("*")
public class FichierController {
	
@Autowired
private FichierRepository fichierRepository;

@RequestMapping(value="/listeFichier")
public ResponseEntity<List<Fichier>> liste(Model model) {
	List<Fichier>fichier=fichierRepository.findAll();
	model.addAttribute("ListeFichier", fichier);
	return new ResponseEntity<List<Fichier>>(fichier, HttpStatus.OK);	
}
@RequestMapping(value="/supprimerFichier")
public ResponseEntity<List<Fichier>> supprimer(Integer id) {
	Fichier fichier=new Fichier();
	fichier.setId(id);
	this.fichierRepository.delete(fichier);
	List<Fichier>lsfichier=fichierRepository.findAll();
	return new ResponseEntity<List<Fichier>>(lsfichier, HttpStatus.OK);	
}


}
