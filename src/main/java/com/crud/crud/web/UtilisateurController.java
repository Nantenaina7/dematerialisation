package com.crud.crud.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.crud.crud.Dossier;
import com.crud.crud.Utilisateurs;
import com.crud.crud.dao.UtilisateurRepository;

@Controller
@CrossOrigin("*")
public class UtilisateurController {
	@Autowired
private UtilisateurRepository utilisateurRepository;
	
	@RequestMapping(value="/listeUtilisateurs")
	
	public ResponseEntity<List<Utilisateurs>> liste() {
		List<Utilisateurs>utilisateurs=utilisateurRepository.findAll();
		
		return new ResponseEntity<List<Utilisateurs>>(utilisateurs, HttpStatus.OK);	
	}
	
	@RequestMapping(value="/supprimerUtilisateurs")
	public ResponseEntity<List<Utilisateurs>> supprimer(Integer id) {
		Utilisateurs utilisateurs=new Utilisateurs();
		utilisateurs.setId(id);
		this.utilisateurRepository.delete(utilisateurs);
		List<Utilisateurs>lsutilisateurs=utilisateurRepository.findAll();
		return new ResponseEntity<List<Utilisateurs>>(lsutilisateurs, HttpStatus.OK);	
	}
	
	@RequestMapping(value="/saveUtilisateurs")
	public ResponseEntity<List<Utilisateurs>> modifier(@RequestParam Utilisateurs utilisateurs) {
		
		this.utilisateurRepository.save(utilisateurs);
		List<Utilisateurs>lsutilisateurs=utilisateurRepository.findAll();
		return new ResponseEntity<List<Utilisateurs>>(lsutilisateurs, HttpStatus.OK);	
	}
	@RequestMapping(value="/rechercheUtilisateurs")
	public ResponseEntity<List<Utilisateurs>> rechercherUtilisateurs(@RequestParam(name = "",defaultValue = "")String titreDossie ,@RequestParam(name="nom",defaultValue = "0" )String nom) {
		
		List<Utilisateurs> lsutilisateur= this.utilisateurRepository.findAll();
		return new ResponseEntity<List<Utilisateurs>>(lsutilisateur, HttpStatus.OK);	
}
}
