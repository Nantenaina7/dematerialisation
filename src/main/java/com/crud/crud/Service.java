package com.crud.crud;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Service implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id	@GeneratedValue(strategy=GenerationType.AUTO)	
	private Integer id;
	@Column
	private String nom_service;
	
	public Service(int id, String nom_service) {
		super();
		this.id = id;
		this.nom_service = nom_service;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom_service() {
		return nom_service;
	}
	public void setNom_service(String nom_service) {
		this.nom_service = nom_service;
	}

}
