package com.crud.crud.dao;

import com.crud.crud.Dossier;
import com.crud.crud.Service;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ServiceRepository extends JpaRepository <Service, Integer> {

}
