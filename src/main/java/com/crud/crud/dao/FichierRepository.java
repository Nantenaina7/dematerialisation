package com.crud.crud.dao;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.crud.crud.Dossier;
import com.crud.crud.Fichier;
public interface FichierRepository extends JpaRepository<Fichier, Integer> {

}
