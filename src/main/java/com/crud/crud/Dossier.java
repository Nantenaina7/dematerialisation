package com.crud.crud;

import javax.persistence.Entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class Dossier implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id	@GeneratedValue(strategy=GenerationType.AUTO)
	
	private int id;
	@Column
	private  String titre;
	@Column
	private int classement;
	@Column
	private Date date_creation;
	@Column
	private String chemin;
	
	
	public Dossier() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Dossier(String titre, int classement, Date date_creation, String chemin) {
		super();
		this.titre = titre;
		this.classement = classement;
		this.date_creation = date_creation;
		this.chemin = chemin;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	public int getClassement() {
		return classement;
	}
	public void setClassement(int classement) {
		this.classement = classement;
	}
	public Date getDate_creation() {
		return date_creation;
	}
	public void setDate_creation(Date date_creation) {
		this.date_creation = date_creation;
	}
	public String getChemin() {
		return chemin;
	}
	public void setChemin(String chemin) {
		this.chemin = chemin;
	}
	
	

}
