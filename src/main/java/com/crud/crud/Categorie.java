package com.crud.crud;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Categorie {
	private static final long serialVersionUID = 1L;
	@Id	@GeneratedValue(strategy=GenerationType.AUTO)
	
	@Column
	private int id;
	@Column
	private String nom_categorie;

	public Categorie() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Categorie(String nom_categorie) {
		super();
		this.nom_categorie = nom_categorie;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom_categorie() {
		return nom_categorie;
	}
	public void setNom_categorie(String nom_categorie) {
		this.nom_categorie = nom_categorie;
	}
	

}
