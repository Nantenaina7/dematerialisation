package com.crud.crud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import com.crud.crud.dao.Type_userRepository;
import com.crud.crud.dao.UtilisateurRepository;



@SpringBootApplication
public class DematerialisationApplication {

	public static void main(String[] args) {
		ApplicationContext ctx= SpringApplication.run(DematerialisationApplication.class, args);
	UtilisateurRepository  utilisateursRepository= ctx.getBean(UtilisateurRepository.class);
	Type_userRepository type_userRepository= ctx.getBean(Type_userRepository.class);
	
	Type_user type=new Type_user("User");

	Utilisateurs user= new Utilisateurs("nantenaina","andrianary",0001,"iavoloha","nandriams@gmail.com","0326079597","dsi","informaticienne","nante",type);
	Utilisateurs user2= new Utilisateurs("asus","asuss",0002,"usa","asus@gmail.com","0326079554","dsi","informaticienne","aus",type);
	Utilisateurs user3= new Utilisateurs("spring","spreing",0003,"iavoloha","spring@gmail.com","0326077497","dsi","informaticienne","spring",type);
	Utilisateurs user4= new Utilisateurs("sprig","sreing",0400,"iavoloha","sprin@gmail.com","032677497","dsi","informatiienne","sring",type);
	type_userRepository.save(type);
	utilisateursRepository.save(user);
	utilisateursRepository.save(user2);
	utilisateursRepository.save(user3);
	utilisateursRepository.save(user4);
	utilisateursRepository.findAll().forEach(u->System.out.println("Nom:"+u.getNom() +" Adresse :"+u.getAdresse()+" Fontion : "+u.getFonction()));

	}
}
